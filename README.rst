#########################
sphinxcontrib-git-context
#########################

|pipeline| |coverage|

.. |pipeline| image:: https://gitlab.com/ddidier/sphinxcontrib-git-context/badges/master/pipeline.svg
    :target: https://gitlab.com/ddidier/sphinxcontrib-git-context/commits/master
    :alt: Pipeline Status

.. |coverage| image:: https://gitlab.com/ddidier/sphinxcontrib-git-context/badges/master/coverage.svg
    :target: https://gitlab.com/ddidier/sphinxcontrib-git-context/commits/master
    :alt: Coverage Report


A `Sphinx <https://www.sphinx-doc.org>`_ plugin to get the version and the release settings from the GIT repository.

Documentation is available at https://ddidier.gitlab.io/sphinxcontrib-git-context/.
